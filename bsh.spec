Name:           bsh
Version:        2.0
Release:        16.b6
Epoch:          0
Summary:        A Java scripting environment (command-line version)
License:        ASL 2.0 and BSD-3-Clause and Public Domain
URL:            http://www.beanshell.org/
BuildArch:      noarch
Source0:        %{name}-%{version}-b6.tar.gz
Source1:        %{name}-desktop.desktop
BuildRequires:  javapackages-local ant bsf junit javacc glassfish-servlet-api ImageMagick desktop-file-utils
Requires:       java-headless bsf jline javapackages-tools
Provides:       %{name}-utils = %{epoch}:%{version}-%{release}
Obsoletes:      %{name}-utils < 0:2.0 %{name}-demo < 0:2.0
Provides:       bundled(objectweb-asm) = 1.3.6

%description
BeanShell is a small, embeddable, Java source interpreter with object
scripting language features, written in Java. BeanShell executes standard
Java statements and expressions, in addition to obvious scripting commands
and syntax. You can also script objects as simple method closures like those
in Perl and JavaScript(tm).bsh starts the command-line version of BeanShell.

%package        help
Summary:        This package contains help documents
Requires:       %{name} = %{version}-%{release}
Provides:       bsh-manual = %{version}-%{release} bsh-javadoc = %{version}-%{release}
Obsoletes:      bsh-manual < %{version}-%{release} bsh-javadoc < %{version}-%{release}

%description      help
Files for help with bsh.

%prep
%autosetup -n beanshell-%{version}b6 -p1
sed -i 's,org.apache.xalan.xslt.extensions.Redirect,http://xml.apache.org/xalan/redirect,' docs/manual/xsl/*.xsl
%mvn_alias :bsh bsh:bsh bsh:bsh-bsf org.beanshell:bsh
%mvn_file : %{name}

%build
install -d lib
build-jar-repository lib bsf javacc junit glassfish-servlet-api
ant test dist

%install
%mvn_artifact pom.xml dist/%{name}-%{version}b6.jar
%mvn_install -J javadoc
desktop-file-install --mode=644 --dir=%{buildroot}%{_datadir}/applications %{SOURCE1}
install -d -m 755 %{buildroot}%{_datadir}/pixmaps
convert src/bsh/util/lib/icon.gif %{buildroot}%{_datadir}/pixmaps/bsh.png
install -d -m 755 %{buildroot}%{_datadir}/%{name}/webapps
install -m 644 dist/bshservlet.war %{buildroot}%{_datadir}/%{name}/webapps
install -m 644 dist/bshservlet-wbsh.war %{buildroot}%{_datadir}/%{name}/webapps
mkdir %{buildroot}%{_bindir}
%jpackage_script bsh.Interpreter "\${BSH_DEBUG:+-Ddebug=true}" jline.console.internal.ConsoleRunner %{name}:jline %{name} true
%jpackage_script bsh.Console "\${BSH_DEBUG:+-Ddebug=true}" "" bsh bsh-console true
echo '#!%{_bindir}/bsh' > %{buildroot}%{_bindir}/bshdoc
cat scripts/bshdoc.bsh >> %{buildroot}%{_bindir}/bshdoc

%files -f .mfiles
%doc LICENSE
%attr(0755,root,root) %{_bindir}/%{name}*
%{_datadir}/applications/%{name}-desktop.desktop
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/%{name}

%files help -f .mfiles-javadoc
%doc NOTICE README.md src/Changes.html src/CodeMap.html docs/faq/faq.html
%doc docs/manual/html
%doc docs/manual/images/*.jpg
%doc docs/manual/images/*.gif

%changelog
* Tue May 10 2022 xu_ping <xuping33@h-partners.com> - 0:2.0-16.b6
- License compliance rectification

* Fri Feb 28 2020 daiqianwen <daiqianwen@huawei.com> - 0:2.0-15.b6
- modify spec

* Fri Dec 13 2019 gulining<gulining1@huawei.com> - 0:2.0-14.b6
- Pakcage init
